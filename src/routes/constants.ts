type UrlType = {
  Home: string;
  Detail: string;
};

export const Router: UrlType = {
  Home: "/",
  Detail: "/detail/:id",
};
